// ===== Données ======
const width_boite = 60;
const x_origin = 0;
const popup = document.querySelector("#popup");
const algorithmes = document.querySelector("#algorithmes");
const algorithme = document.querySelector("#algorithme tbody");
const variables = document.querySelector("#variables tbody");
const comp = document.querySelector("#comparaison");

let steps = [];
let marker = 0;
let nbComparaisons = 0;
// ==============================
//       CANVAS 1
// ==============================

const textePanel = document.getElementById("textePanel");
const motifPanel = document.getElementById("motifPanel");

let motifContainer = null;

function prepare(){
    textePanel.innerHTML = "";
    motifPanel.innerHTML = "";

    for (let i = 0; i < texte.length; i++){
        let boite = document.createElement("div");
        boite.classList.add("boite");
        boite.innerHTML = texte[i];
        textePanel.appendChild(boite);
    }

    for (let i = 0; i < motif.length; i++){
        let boite = document.createElement("div");
        boite.classList.add("boite");
        boite.innerHTML = motif[i];
        motifPanel.appendChild(boite);
    }
}

popup.addEventListener('click', function(){
  this.style.display = "none";
});

function getPreviousValue(variable){
    let indice = marker;
    while (indice >= 0 && !steps[indice].hasOwnProperty(variable)){
        indice--;
    }
    if (indice < 0)
        return "";
    return steps[indice][variable];
}

document.querySelector("#start").addEventListener('click', start);

document.querySelector("#previous").addEventListener('click', function(){
    if (marker >= 0){
        marker = Math.max(marker - 2, 0);
        algorithme.querySelector(".marker").classList.remove("marker");
        algorithme.children[steps[marker].line].children[0].classList.add("marker");        
        algos[algorithmes.value].variables.forEach(v => {
            document.querySelector("#v_" + v).innerHTML = getPreviousValue(v);
        });
        let i = getPreviousValue("i");
        let j = getPreviousValue("j");
        motifPanel.style.marginLeft = (i * 62) + "px";
        marker++;
    }
});

document.querySelector("#next").addEventListener('click', function(){
    if (marker < steps.length) {
        algorithme.querySelector(".marker").classList.remove("marker");
        algorithme.children[steps[marker].line].children[0].classList.add("marker");
        algos[algorithmes.value].variables.forEach(v => {
            if (steps[marker].hasOwnProperty(v)) {
                document.querySelector("#v_" + v).innerHTML = steps[marker][v];
            }
        });
        let i = getPreviousValue("i");
        let j = getPreviousValue("j");
        let trouve = getPreviousValue("trouve");
        if (steps[marker].comparaison && j >= 0  && j < motif.length){
            nbComparaisons++;
            comp.innerHTML = nbComparaisons;
            motifPanel.children[j].classList.add("ok");
            let comparaison = (texte[i + j] == motif[j])?"ok":"nok";
            textePanel.children[i + j].classList.add(comparaison);
        } else if (steps[marker].reset && !trouve){
            document.querySelectorAll(".ok").forEach(function(boite){
                boite.classList.remove("ok");
            });
            document.querySelectorAll(".nok").forEach(function(boite){
                boite.classList.remove("nok");
            });
        }
        motifPanel.style.marginLeft = (i * 52) + "px";
        marker++;
    }
});

function helpContexte(){
    return "<div class='block'> <h1>Algorithmes de recherche textuelle</h1><div class='row'><div class='col' style='width: 66%;'><p>La recherche textuelle consiste en la recherche des occurences d'une suite de caractères, appelée le <b>motif</b>, dans une autre suite de caractères plus longue, appelée le <b>texte</b></p><p>Les applications sont nombreuses allant de la simple recherche d'un mot dans un document, de mots clés dans une multitude de pages web, jusqu'à la recherche de l'enchaînements des bases nucléiques dans les modules d'ADN.</p></div><div class='col'><img src='https://pixees.fr/informatiquelycee/n_site/img/nsi_term_algo_boyer_adn.gif' width='90%'/></div></div></div>";
}

function helpAlgo() {
    return algos[algorithmes.value].help;
}

document.querySelectorAll(".help").forEach(e => {
    e.addEventListener('click', function () {
        document.querySelector("#popup-message").innerHTML = eval(e.dataset["action"]);
        popup.style.display = "flex";
    });
});

function updateAlgo() {
    algorithme.innerHTML = "<tr><td class='marker'></td><td colspan=2>Début</td></tr>";
    const algo = algos[algorithmes.value];
    algo.descriptif.forEach((ligne, index) => {
        let tr = document.createElement('tr');
        let td = document.createElement('td');
        td.innerHTML = "";
        tr.appendChild(td);
        td = document.createElement('td');
        td.innerHTML = index + 1;
        tr.appendChild(td);
        td = document.createElement('td');
        td.innerHTML = ligne;
        tr.appendChild(td);
        algorithme.appendChild(tr);
    });
    let tr = document.createElement('tr');
    let td = document.createElement('td');
    td.innerHTML = "";
    tr.appendChild(td);
    td = document.createElement('td');
    td.colspan = 2;
    td.innerHTML = "Fin";
    tr.appendChild(td);
    algorithme.appendChild(tr);

    variables.innerHTML = "";
    algo.variables.forEach(v => {
        let tr = document.createElement('tr');
        let td = document.createElement('th');
        td.innerHTML = "<i>" + v + "</i>";
        tr.appendChild(td);
        td = document.createElement('td');
        td.innerHTML = "";
        td.id = "v_" + v;
        tr.appendChild(td);
        variables.appendChild(tr);
    });
}

function launchAlgo() {
    steps = [];
    marker = 0;
    const algo = algos[algorithmes.value];
    algo.function(texte, motif, steps);
}

function start() {
    marker = 0;
    nbComparaisons = 0;
    comp.innerHTML = nbComparaisons;
    algorithme.querySelector(".marker").classList.remove("marker");
    algorithme.children[0].children[0].classList.add("marker");
    algos[algorithmes.value].variables.forEach(v => {
        document.querySelector("#v_" + v).innerHTML = "";
    });
    motifPanel.style.marginLeft = "0px";
}

document.querySelector("#algorithmes").addEventListener("change", function(){
    prepare();
    updateAlgo();
    launchAlgo();
    start();
});

prepare();
updateAlgo();
launchAlgo();
start();
