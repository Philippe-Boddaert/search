function setToString(G, set) {
    let s = "{";
    set.forEach(v => { s += G.node(v).label() + ", " });
    return s + "}";
}

function dictToString(dict) {
    let s = "<table class='dict'>";
    s += "<thead><tr>";
    Object.keys(dict).forEach(v => {s += "<th>" + v + "</th>";});
    s += "</thead></tr><tbody><tr>";
    Object.keys(dict).forEach(v => {s += "<td class='centered'>" + dict[v] + "</td>";});
    return s + "</tr></tboby></table>";
}

function xileToString(G, xile) {
    let s = "[";
    xile.__structure.forEach(v => { 
        if (typeof v === "object"){
            s += "(" + G.node(v[0]).label() + ", " + setToString(G, v[1]) + "), "; 
        } else {
            s += G.node(v).label() + ", ";
        }
    });
    return s + "]";
}

const algos = {
    "naifGD": {
        "help": "<h1>Algorithme Naïf (Gauche-Droite)</h1><h2>Prototypage</h2><p>Entrées : Cet algorithme nécessite un texte et un motif</p><p>Sortie : Vrai si le motif existe dans le texte, Faux sinon.</p><h2>Principe</h2><p>L'algorithme compare le 1er caractère du motif au 1er caractère du texte.</p><p>Si les caractères sont égaux, l'algorithme poursuit les comparaisons des caractères suivants du motif.</p><p>Si la taille du motif est atteinte, le motif est présent et l'algorithme renvoie Vrai.</p><p>Sinon, l'algorithme décale le motif d'un caractère et recommence les comparaisons.</p>",
        "parameters": [],
        "function": function (texte, motif, steps = []) {
            const n = texte.length;
            steps.push({ "line": 1, "texte": texte, "motif": motif});
            const m = motif.length;
            steps.push({ "line": 2, "n" : n});
            let trouve = false;
            steps.push({ "line": 3, "m" : m});
            let i = 0;
            steps.push({ "line": 4, "trouve" : trouve});
            steps.push({ "line": 5, "i" : i });
            while (!trouve && i <= n - m){
                let j = 0;
                steps.push({ "line": 6});
                steps.push({ "line": 7, "j" : j, "comparaison": true});
                while (j < m && texte[i + j] == motif[j]){
                    j++;
                    steps.push({ "line": 8});
                    steps.push({ "line": 7, "j": j, "comparaison": true});
                }
                steps.push({ "line": 9});
                if (j == m){
                    steps.push({ "line": 10});
                    trouve = true;
                    steps.push({ "line": 5, "trouve" : trouve});
                } else {
                    steps.push({ "line": 11});
                    steps.push({ "line": 12});
                    i++;
                    steps.push({ "line": 5, "i" : i, "reset": true});
                }
            }
            steps.push({ "line": 13});
            return trouve;
        },
        "descriptif": [
            "<i>n</i> &leftarrow; |<i>texte</i>|",
            "<i>m</i> &leftarrow; |<i>motif</i>|",
            "<i>trouve</i> &leftarrow; Faux",
            "<i>i</i> &leftarrow; 0",
            "Tant que <i>trouve</i> est Faux et <i>i</i> &le; <i>n</i> - <i>m</i>",
            "&nbsp;&nbsp;<i>j</i> &leftarrow; 0",
            "&nbsp;&nbsp;Tant que <i>j</i> &lt; <i>m</i> et <i>texte[i + j]</i> == <i>motif[j]</i>", 
            "&nbsp;&nbsp;&nbsp;&nbsp;<i>j</i> &leftarrow; <i>j</i> + 1",
            "&nbsp;&nbsp;Si <i>j</i> = <i>m</i> alors",
            "&nbsp;&nbsp;&nbsp;&nbsp;<i>trouve</i> &leftarrow; Vrai",
            "&nbsp;&nbsp;Sinon",
            "&nbsp;&nbsp;&nbsp;&nbsp;<i>i</i> &leftarrow; <i>i</i> + 1",
            "Renvoyer <i>trouve</i>"
        ],
        "variables": ["texte", "motif", "n", "m", "i", "j", "trouve"]
    },
    "naifDG": {
        "help": "<h1>Algorithme naïf (Droite-Gauche)</h1><h2>Prototypage</h2><p>Entrées : Cet algorithme nécessite un texte et un motif</p><p>Sortie : Vrai si le motif existe dans le texte, Faux sinon.</p><h2>Principe</h2><p>L'algorithme compare le dernière caractère du motif au caractère du texte au même indice.</p><p>Si les caractères sont égaux, l'algorithme poursuit les comparaisons des caractères précédents du motif.</p><p>Si l'entièreté du motif est comparé, le motif est présent et l'algorithme renvoie Vrai.</p><p>Sinon, l'algorithme décale le motif d'un caractère et recommence les comparaisons.</p>",
        "parameters": [],
        "function": function (texte, motif, steps = []) {
            const n = texte.length;
            steps.push({ "line": 1, "texte": texte, "motif": motif});
            const m = motif.length;
            steps.push({ "line": 2, "n" : n});
            let trouve = false;
            steps.push({ "line": 3, "m" : m});
            let i = 0;
            steps.push({ "line": 4, "trouve" : trouve});
            steps.push({ "line": 5, "i" : i });
            while (!trouve && i <= n - m){
                let j = m - 1;
                steps.push({ "line": 6});
                steps.push({ "line": 7, "j" : j, "comparaison": true});
                while (j >= 0 && texte[i + j] == motif[j]){
                    j--;
                    steps.push({ "line": 8});
                    steps.push({ "line": 7, "j": j, "comparaison": true});
                }
                steps.push({ "line": 9});
                if (j < 0){
                    steps.push({ "line": 10});
                    trouve = true;
                    steps.push({ "line": 5, "trouve" : trouve});
                } else {
                    steps.push({ "line": 11});
                    steps.push({ "line": 12});
                    i++;
                    steps.push({ "line": 5, "i" : i, "reset": true});
                }
            }
            steps.push({ "line": 13});
            return trouve;
        },
        "descriptif": [
            "<i>n</i> &leftarrow; |<i>texte</i>|",
            "<i>m</i> &leftarrow; |<i>motif</i>|",
            "<i>trouve</i> &leftarrow; Faux",
            "<i>i</i> &leftarrow; 0",
            "Tant que <i>trouve</i> est Faux et <i>i</i> &le; <i>n</i> - <i>m</i>",
            "&nbsp;&nbsp;<i>j</i> &leftarrow; <i>m</i> - 1",
            "&nbsp;&nbsp;Tant que <i>j</i> &ge; 0 et <i>texte[i + j]</i> == <i>motif[j]</i>", 
            "&nbsp;&nbsp;&nbsp;&nbsp;<i>j</i> &leftarrow; <i>j</i> - 1",
            "&nbsp;&nbsp;Si <i>j</i> &lt; 0 alors",
            "&nbsp;&nbsp;&nbsp;&nbsp;<i>trouve</i> &leftarrow; Vrai",
            "&nbsp;&nbsp;Sinon",
            "&nbsp;&nbsp;&nbsp;&nbsp;<i>i</i> &leftarrow; <i>i</i> + 1",
            "Renvoyer <i>trouve</i>"
        ],
        "variables": ["texte", "motif", "n", "m", "i", "j", "trouve"]
    },
    "boyerMoore": {
        "help": "<h1>Algorithme de Boyer-Moore-Horspool</h1><h2>Prototypage</h2><p>Entrées : Cet algorithme nécessite un texte et un motif</p><p>Sortie : Vrai si le motif existe dans le texte, Faux sinon.</p><h2>Principe</h2><p>L'algorithme utilise un pré-traitement du motif afin de calculer le saut maximum à effectuer après avoir trouvé une non-concordance.</p><p>Durant la recherche, la comparaison se fait de la droite vers la gauche comme l'algorithme naïf (Droite-Gauche).</p><p>En cas d'une non-concordance, le décalage du motif n'est pas de un mais du saut indiqué dans la table de décalage.</p>",
        "parameters": [],
        "function": function (texte, motif, steps = []) {
            function construire_decalages(motif){
                let dico = {};
                let m = motif.length;
                for (let lettre of motif){
                    dico[lettre] = m
                }
            
                for (let i = 0; i < m - 1; i++){
                    dico[motif[i]] = m - i - 1;
                }
                return dico;
            } 
            const n = texte.length;
            steps.push({ "line": 1, "texte": texte, "motif": motif});
            const m = motif.length;
            steps.push({ "line": 2, "n" : n});
            const decalages = construire_decalages(motif);
            steps.push({ "line": 3, "m" : m});
            let trouve = false;
            let i = 0;
            steps.push({ "line": 4, "decalage" : dictToString(decalages)});
            steps.push({ "line": 5, "trouve" : trouve});
            steps.push({ "line": 6, "i" : i });
            while (!trouve && i <= n - m){
                let j = m - 1;
                steps.push({ "line": 7});
                steps.push({ "line": 8, "j" : j, "comparaison": true});
                while (j >= 0 && texte[i + j] == motif[j]){
                    j--;
                    steps.push({ "line": 9});
                    steps.push({ "line": 8, "j": j, "comparaison": true});
                }
                steps.push({ "line": 10});
                if (j < 0){
                    steps.push({ "line": 11});
                    trouve = true;
                    steps.push({ "line": 6, "trouve" : trouve});
                } else {
                    steps.push({ "line": 12});
                    steps.push({ "line": 13});
                    i = i + (decalages.hasOwnProperty(texte[i + j])?decalages[texte[i + j]]:m) - ( m - 1 - j);
                    steps.push({ "line": 6, "i" : i, "reset": true});
                }
            }
            steps.push({ "line": 14});
            return trouve;
        },
        "descriptif": [
            "<i>n</i> &leftarrow; |<i>texte</i>|",
            "<i>m</i> &leftarrow; |<i>motif</i>|",
            "<i>decalage</i> &leftarrow; construireTable(m)",
            "<i>trouve</i> &leftarrow; Faux",
            "<i>i</i> &leftarrow; 0",
            "Tant que <i>trouve</i> est Faux et <i>i</i> &le; <i>n</i> - <i>m</i>",
            "&nbsp;&nbsp;<i>j</i> &leftarrow; <i>m</i> - 1",
            "&nbsp;&nbsp;Tant que <i>j</i> &ge; <i>0</i> et <i>texte[i + j]</i> == <i>motif[j]</i>", 
            "&nbsp;&nbsp;&nbsp;&nbsp;<i>j</i> &leftarrow; <i>j</i> - 1",
            "&nbsp;&nbsp;Si <i>j</i> &lt; <i>0</i> alors",
            "&nbsp;&nbsp;&nbsp;&nbsp;<i>trouve</i> &leftarrow; Vrai",
            "&nbsp;&nbsp;Sinon",
            "&nbsp;&nbsp;&nbsp;&nbsp;<i>i</i> &leftarrow; <i>i</i> + decalage[texte[i + j]] - (<i>m</i> - 1 - <i>j</i>)",
            "Renvoyer <i>trouve</i>"
        ],
        "variables": ["texte", "motif", "decalage", "n", "m", "i", "j", "trouve"]
    }
}